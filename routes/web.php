<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CSVController;
use App\Http\Controllers\ImportController;
Route::get('/', [CSVController::class, 'index']);


Route::post('/import', [ImportController::class, 'import']);
Route::get('/filter', [CSVController::class, 'filter']);


