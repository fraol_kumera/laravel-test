<?php

namespace App\Http\Controllers;

use App\Models\CsvData;
use Illuminate\Http\Request;

class CSVController extends Controller
{
    public function index(Request $request)
    {

        $data = CsvData::paginate(10);
        return view('CSV.index', compact('data'));
    }



public function filter(Request $request)
{
    $query = CsvData::query();

    // Apply filters if provided in the request
    if ($request->category) {
        $query->where('category', $request->category);
    }

    if ($request->gender) {
        $query->where('gender', $request->gender);
    }

    if ($request->birthDate) {
        $query->where('birthDate', $request->birthDate);
    }

    if ($request->age) {
        $query->whereRaw('YEAR(CURDATE()) - YEAR(birthDate) = ?', [$request->age]);
    }

    if ($request->age_range) {
        [$min_age, $max_age] = explode('-', $request->age_range);
        $query->whereRaw('YEAR(CURDATE()) - YEAR(birthDate) BETWEEN ? AND ?', [$min_age, $max_age]);
    }

    $data = $query->select(['category', 'firstname', 'lastname', 'email', 'gender', 'birthDate'])
                 ->orderBy('category', 'ASC')
                 ->orderBy('firstname', 'ASC')
                 ->paginate(10);
                 return view('CSV.index', compact('data'));
   // return response()->json($data);
}

}
