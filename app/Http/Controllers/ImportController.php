<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Csv\Reader;
use App\Models\CsvData;

class ImportController extends Controller
{
    public function import(Request $request)
    {
        // Validate the uploaded file
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,txt'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        // Read the CSV file
        $csv = Reader::createFromPath($request->file('file')->getPathname());
        $csv->setHeaderOffset(0);

        // Loop through each row and insert data into the csv_data table
        foreach ($csv as $row) {
              
                $newDate = \Carbon\Carbon::createFromFormat('m/d/Y', $row['birthDate'],)->format('Y-m-d');
            CsvData::create([
                'category' => $row['category'],
                'firstname' => $row['firstname'],
                'lastname' => $row['lastname'],
                'email' => $row['email'],
                'gender' => $row['gender'],
                'birthDate' => $newDate,
            ]);
        }

        // Redirect back to the form with a success message
        return redirect()->back()->with('success', 'CSV file imported successfully.');
    }
}
