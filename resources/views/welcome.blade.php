@extends('layouts.app')

@section('content')
    <h1>CSV Data</h1>

    <form action="" method="GET">
        <div class="form-group">
            <label for="category">Category:</label>
            <select name="category" id="category" class="form-control">
                <option value="">All</option>
                <option value="Sports" {{ request('category') == 'Sports' ? 'selected' : '' }}>Sports</option>
                <option value="Music" {{ request('category') == 'Music' ? 'selected' : '' }}>Music</option>
                <option value="Movies" {{ request('category') == 'Movies' ? 'selected' : '' }}>Movies</option>
            </select>
        </div>

        <div class="form-group">
            <label for="gender">Gender:</label>
            <select name="gender" id="gender" class="form-control">
                <option value="">All</option>
                <option value="Male" {{ request('gender') == 'Male' ? 'selected' : '' }}>Male</option>
                <option value="Female" {{ request('gender') == 'Female' ? 'selected' : '' }}>Female</option>
            </select>
        </div>

        <div class="form-group">
            <label for="birthDate">Date of Birth:</label>
            <input type="date" name="birthDate" id="birthDate" class="form-control" value="{{ request('birthDate') }}">
        </div>

        <div class="form-group">
            <label for="age">Age:</label>
            <input type="number" name="age" id="age" class="form-control" value="{{ request('age') }}">
        </div>

        <div class="form-group">
            <label for="age_range">Age Range:</label>
            <select name="age_range" id="age_range" class="form-control">
                <option value="">All</option>
                <option value="18-25" {{ request('age_range') == '18-25' ? 'selected' : '' }}>18-25</option>
                <option value="26-35" {{ request('age_range') == '26-35' ? 'selected' : '' }}>26-35</option>
                <option value="36-45" {{ request('age_range') == '36-45' ? 'selected' : '' }}>36-45</option>
                <option value="46-55" {{ request('age_range') == '46-55' ? 'selected' : '' }}>46-55</option>
                <option value="56+" {{ request('age_range') == '56+' ? 'selected' : '' }}>56+</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Filter</button>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th>Category</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Date of Birth</th>
            </tr>
        </thead>
        <tbody>
            {{-- @foreach ($data as $item) --}}
                <tr>
                    <td>catagory</td>
                    <td>first name</td>
                    <td>last name</td>
                    <td>email</td>
                    <td>gender</td>
                    <td>birth date</td>
                </tr>
            {{-- @endforeach --}}
        </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{-- {{ $data->appends(request()->query())->links() }} --}}
    </div>
@endsection
