@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">


                <div class="panel-body">

                    <form action="/import" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        @csrf
                        <input type="file" name="file">
                        <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" required>Import CSV</button>
                    </div>
                </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="col-md-7">




                        <form action="/filter" method="GET">
                            <div class="container">
                                <div class="row">
                                <div class="col-md-2">
                                    <label for="category">Category</label>
                                    <select class="form-control" id="category" name="category">
                                        <option value="">All</option>
                                        @foreach ($data as $item)
                                        <option name="category" value="{{ $item->category }}">{{ $item->category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="gender">Gender</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">All</option>
                                        <option value="male" {{ request()->query('gender') === 'male' ? 'selected' : '' }}>Male</option>
                                        <option value="female" {{ request()->query('gender') === 'female' ? 'selected' : '' }}>Female</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="birthdate">Date of Birth</label>
                                    <input type="date" class="form-control" id="birthdate"  name="date_of_birth" value="{{ request()->query('date_of_birth') }}">
                                </div>

                                <div class="col-md-2">
                                    <label for="age_range">Age Range</label>
                                    <select class="form-control" id="age_range" name="age_to" value="{{ request()->query('age_to') }}">
                                        <option value="">All</option>
                                        <option value="18-25">18-25</option>
                                        <option value="26-35">26-35</option>
                                        <option value="36-45">36-45</option>
                                        <option value="46-55">46-55</option>
                                        <option value="56-65">56-65</option>
                                        <option value="66-75">66-75</option>
                                        <option value="76-85">76-85</option>
                                        <option value="86-95">86-95</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary mt-4">Filter</button>
                                </div>
                        </form>
                    </div>
            </div>


        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">


                <div class="panel-body">
<table id="products-table" class="table table-striped form-group" class="display" style="width:90%">
        <thead>
            <tr>

                    <th>Category</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Date of Birth</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td>{{ $item->category }}</td>
                    <td>{{ $item->firstname }}</td>
                    <td>{{ $item->lastname }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->gender }}</td>
                    <td>{{ $item->birthDate }}</td>
                </tr>
            @endforeach
        </tbody>

    </table>
    <div class="d-flex justify-content-center">
        {{ $data->links() }}
    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection
@section('scripts')
    <script>
$(document).ready(function () {
    $('#example').DataTable();
});
</script>
@endsection
