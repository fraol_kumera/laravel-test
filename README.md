Laravel CSV Importer
The Laravel CSV Importer is a simple application that allows you to import data from a CSV file into your Laravel application's database.

Installation
Clone this repository to your local machine.
Run composer install to install the required dependencies.
Rename the .env.example file to .env and update the database connection settings with your own credentials.
Run php artisan key:generate to generate a new application key.
Run php artisan migrate to create the necessary database tables.
Usage
Place your CSV file in the storage/app/csv directory.

Select the CSV file you want to import and click "Import".
Wait for the import to finish.
Check your database to ensure the data was successfully imported.
Customization
If you want to customize the import process, you can modify the App\Imports\YourModelImport class. This class is responsible for parsing the CSV file and inserting the data into the database.

Credits
This application was created by Fraol Kumera.

